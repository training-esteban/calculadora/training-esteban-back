package io.quind.calculadora;

import io.quind.calculadora.model.ResultModel;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class CalculadoraApplicationTests {
	//Arr
	float num1 = 3;
	float num2 = 5;
	float result;
	@Test
	void addTest() {
		ResultModel res = new ResultModel();
		result = 8;
		res.setResult(num1+num2);
		res.setStatus("success");
		assertEquals(result, res.getResult());
	}

}
