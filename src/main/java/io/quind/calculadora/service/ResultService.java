package io.quind.calculadora.service;

import io.quind.calculadora.model.ResultModel;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class ResultService {
    //http://localhost:9090/calculadora/sumar?num1=153432&num2=23567
    @RequestMapping("/calculadora/sumar")
    public ResultModel getAdd(@RequestParam(name="num1", defaultValue = "0") float num1, @RequestParam(name="num2", defaultValue = "0") float num2){
        ResultModel res = new ResultModel();
        float resultado = 0;
        resultado = num1+num2;
        resultado = num1+num2;
        resultado = num1+num2;
        res.setResult(resultado);
        res.setStatus("success");
        return res;
    }
    @RequestMapping("/calculadora/restar")
    public ResultModel getSub(@RequestParam(name="num1", defaultValue = "0") float num1, @RequestParam(name="num2", defaultValue = "0") float num2){
        ResultModel res = new ResultModel();
        res.setResult(num1-num2);
        res.setStatus("success");
        return res;
    }
    @RequestMapping("/calculadora/multiplicar")
    public ResultModel getMul(@RequestParam(name="num1", defaultValue = "0") float num1, @RequestParam(name="num2", defaultValue = "0") float num2){
        ResultModel res = new ResultModel();
        res.setResult(num1*num2);
        res.setStatus("success");
        return res;
    }
    @RequestMapping("/calculadora/dividir")
    public ResultModel getDiv(@RequestParam(name="num1", defaultValue = "0") float num1, @RequestParam(name="num2", defaultValue = "0") float num2){
        ResultModel res = new ResultModel();
        res.setResult(num1/num2);
        if (num2!=0){
            res.setStatus("success");
        }else {
            res.setStatus("error");
        }
        return res;
    }
    //{result:8, status:success}
    //{result:0, status:failed}
}
