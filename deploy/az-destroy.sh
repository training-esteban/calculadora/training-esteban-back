
az resource delete -g $RESOURCEGROUP \
    --name $STORAGEACCOUNTNAME \
    --resource-type "Microsoft.Storage/storageAccounts";