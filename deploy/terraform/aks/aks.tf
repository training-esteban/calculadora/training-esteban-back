provider "azurerm" {
  subscription_id            = "6fea5c6b-073f-4a0d-9a56-51e719c4b283"
  client_id                  = "fc289457-8fd9-4e55-bf34-79c2e299b386"
  client_secret              = "jzIG-nR3E_j84j3pZL.HpP~jTW_Bj2UMnU"
  tenant_id                  = "f2567840-b2b7-49ad-8240-568a42281394"
  skip_provider_registration = true
  features {}
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.21.0"
    }
  }
  # Blob storage account. Authenticating using service principal
  backend "azurerm" {
    resource_group_name  = "resource-group-esteban-pulgarin"
    storage_account_name = "estebanquindstorage"
    container_name       = "storagecontaineraks"
    key                  = "terraform.tfstate"
  }
}
# Data resource
data "azurerm_client_config" "current" {}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.dns_prefix

  linux_profile {
    admin_username = "Esteban_Training"

    ssh_key {
      key_data = file(var.ssh_public_key)
    }
  }
  default_node_pool {
    name       = "agentpool"
    node_count = var.agent_count
    vm_size    = "Standard_D2_v2"
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  network_profile {
    load_balancer_sku = "Standard"
    network_plugin    = "kubenet"
  }

  tags = {
    Environment = "Development"
  }
}

provider "kubernetes" {
  host                   = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
}

resource "kubernetes_namespace" "example" {
  depends_on = [azurerm_kubernetes_cluster.k8s]
  metadata {
    name = "namespace-esteban"
  }
}