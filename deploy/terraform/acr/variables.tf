variable "resource_group_name" {
	default = "resource-group-esteban-pulgarin"
}

variable "location" {
	default = "westus3"
}

variable "acr_name" {
	default = "acrquindesteban"
}
