provider "azurerm" {
  features {}
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.21.0"
    }
  }
  # Blob storage account. Authenticating using service principal
  backend "azurerm" {
    resource_group_name  = "resource-group-esteban-pulgarin"
    storage_account_name = "estebanquindstorage"
    container_name       = "storagecontaineracr"
    key                  = "terraform.tfstate"
  }
}
# Data resource
data "azurerm_client_config" "current" {}
# Resource container registry
resource "azurerm_container_registry" "acr" {
  name                = var.acr_name
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Basic"
  admin_enabled       = true
}
