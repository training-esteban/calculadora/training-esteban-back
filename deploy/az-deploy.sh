#!/bin/bash
STACCOUNT=$(az storage account check-name --name $2);
VALUE=$(jq -r '.nameAvailable' <<< $STACCOUNT);

if [[ "$VALUE" == "true" ]]; then
az storage account create \
--name $2 \
--resource-group $1 \
--location westus3 \
--sku Standard_RAGRS \
--kind StorageV2; 

JSON=$(az storage account keys list \
--resource-group $1 \
--account-name $2);

KEYST=( $(jq -r '.[0].value' <<< $JSON) );
                            
az storage container create -n $3 --account-name $2 --account-key $KEYST;     
az storage container create -n $4 --account-name $2 --account-key $KEYST;

else

JSON=$(az storage account keys list \
--resource-group $1 \
--account-name $2);

KEYST=( $(jq -r '.[0].value' <<< $JSON) ) 
                            
az storage container create -n $3 --account-name $2 --account-key $KEYST;    
az storage container create -n $4 --account-name $2 --account-key $KEYST;

fi